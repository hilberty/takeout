class Utf8Generator < Rails::Generators::Base
	def insert_utf8_to_models_and_controllers
		Dir.foreach('app/models/') do |f|
			if f.match(/rb/)
				inject_into_file "app/models/#{f}" , "# coding utf-8\n", :before => "class "
			end
		end

		Dir.foreach('app/controllers/') do |f|
			if f.match(/rb/)
				inject_into_file "app/controllers/#{f}" , "# coding: utf-8\n", :before => "class "
			end
		end
	end
end