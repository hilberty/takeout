# coding utf-8
class User < ActiveRecord::Base
	attr_accessible :address, :name, :tel
	validates_presence_of :address, :name
	validates :tel, :length => { :minimum => 7 }

	has_many :orders
end
