# coding utf-8
class Order < ActiveRecord::Base
	attr_accessible :items, :prices, :tel, :user_name, :address
	validates_presence_of :items, :prices, :tel, :user_name, :address
	belongs_to :user
end
