# coding utf-8
class Menu < ActiveRecord::Base
	attr_accessible :description, :name, :photo, :price
	has_attached_file :photo, :styles => { :medium => "260x180>", :thumb => "64x64>" }
	validates_uniqueness_of :name
end
