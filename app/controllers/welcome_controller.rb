# coding: utf-8
class WelcomeController < ApplicationController
	respond_to :html, :json
	before_filter :auth

	def index
		@menus = Menu.order('id desc')
		@order = Order.new
		@order_form = true
		respond_with(@menus)
	end

	def show
		@menu = Menu.find(params[:id])
		respond_with(@menu)
	end

	def new
		@menu = Menu.new
		respond_with(@menu)
	end

	def edit
		@menu = Menu.find(params[:id])
	end

	def create
		@menu = Menu.new(params[:menu])
		flash[:notice] = 'Menu was successfully created.' if @menu.save
		respond_with(@menu)
	end

	def update
		@menu = Menu.find(params[:id])
		flash[:notice] = 'Menu was successfully updated.' if @menu.update_attributes(params[:menu])
		respond_with(@menu)
	end

	def destroy
		@menu = Menu.find(params[:id])
		@menu.destroy
		respond_with(@menu)
	end
end
