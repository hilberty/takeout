# coding: utf-8
class UsersController < ApplicationController
	respond_to :html, :json
	before_filter :admin, :only => :destroy

	def index
		if @admin
			@users = User.all
		else
			redirect_to '/'
		end
	end

	def show
		@user = User.find(params[:id])
		if !@user = @current_user
			redirect_to new_user_path
		end
	end

	def new
		@user = User.new
		@user.tel = params[:tel]
	end

	def edit
		@user = User.find(params[:id])
	end

	def create
		@user = User.new(params[:user])
		if @user.name != 'Admin' and @user.name != 'admin'
			result = @user.save
		end
		if result
			flash[:notice] = "#{@user.name}，您注册成功了！"
			session[:current_user] = @user
			@current_user = session[:current_user]
			path = '/'
		end
		respond_with(@user)
	end

	def update
		@user = User.find(params[:id])
		flash[:notice] = '用户成功修改了！' if @user.update_attributes(params[:user])
	end

	def destroy
		@user = User.find(params[:id])
		@user.destroy
		redirect_to users_path
	end

	def login
		render 'login', :layout => 'layouts/login'
	end

	def signin
		@user = User.find_by_tel(params[:tel])
		if @user
			session[:current_user] = @user
			if @user.name == 'Admin'
				session[:admin] = true
			end
			redirect_path =  '/'
			notice = '成功登录了！'
		else
			redirect_path =  "/users/new?tel=#{params[:tel]}"
		end

		redirect_to redirect_path, :notice => notice
	end

	def signout
		destroy_session
		redirect_to '/', :notice => '您已退出！'
	end

	def resetadmin
		@admin_user = User.find_by_name('Admin')
		if @admin_user.nil?
			@admin_user = User.new
		end
		logg @admin_user.name
		@admin_user.name = 'Admin'
		@admin_user.tel = 'password' # Set Admin password here, visit /admin everytime you chage password here
		@admin_user.address = 'na'
		result = @admin_user.save

		if result
			flash[:notice] = '修改管理员密码成功！'
		else
			flash[:alert] = '修改管理员密码失败！您的密码可能少于7位数了'
		end

		redirect_to '/'
	end
end
