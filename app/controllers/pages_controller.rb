# coding: utf-8
class PagesController < ApplicationController
	respond_to :html, :json
	before_filter :admin

	def index
		@pages = Page.all
		respond_with(@pages)
	end

	def show
		@page = Page.find(params[:id])
		respond_with(@page)
	end

	def new
		@page = Page.new
		respond_with(@page)
	end

	def edit
		@page = Page.find(params[:id])
	end

	def create
		@page = Page.new(params[:page])
		flash[:notice] = '页面成功创建了！' if @page.save
		respond_with(@page)
	end

	def update
		@page = Page.find(params[:id])
		flash[:notice] = '页面成功修改了！' if @page.update_attributes(params[:page])
		respond_with(@page)
	end

	def destroy
		@page = Page.find(params[:id])
		@page.destroy
		respond_with(@page)
	end
end
