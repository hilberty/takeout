# coding: utf-8
class MenusController < ApplicationController
	respond_to :html, :json
	before_filter :admin

	def index
		@menus = Menu.order('id desc')
		
	end

	def show
		@menu = Menu.find(params[:id])
	end

	def new
		@menu = Menu.new
	end

	def edit
		@menu = Menu.find(params[:id])
	end

	def create
		@menu = Menu.new(params[:menu])
		if @menu.save
			flash[:notice] = '菜单成功创建了！' 
		end
		respond_with @menu
	end

	def update
		@menu = Menu.find(params[:id])
		if @menu.update_attributes(params[:menu])
			flash[:notice] = '菜单成功修改了！' 
			redirect_to menus_path
		end
	end

	def destroy
		@menu = Menu.find(params[:id])
		if @menu.destroy
			
		end
		respond_with @menu
	end

end
