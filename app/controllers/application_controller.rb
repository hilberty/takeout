# coding: utf-8
class ApplicationController < ActionController::Base
	protect_from_forgery

	before_filter :auth

	def logg(info)
		@logg = info
		file_name = 'log/logg.log'
		open(file_name, 'wb') do |file|
			file << info
		end
	end

	def auth
		@current_user = session[:current_user]
		@admin = session[:admin]
	end

	def destroy_session
		session[:current_user] = nil
		session[:admin] = nil
		@current_user = nil
		cookies.delete :_items
	end

	def admin
		if @current_user
			if @current_user.name == 'Admin'
				session[:admin] = true
				@admin = session[:admin]
			else
			end
		end
		redirect_to "/users/login" unless @admin
	end
end
