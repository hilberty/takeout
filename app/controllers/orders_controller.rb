# coding: utf-8
class OrdersController < ApplicationController
	respond_to :html, :json
	before_filter :auth

	def index
		if @admin
			@orders = Order.order('id desc').page(params[:page]).per(500)
		else
			@orders = Order.where("user_id = ?", @current_user.id).order('id desc')
		end

		@orders.each do |order|
			order.items = order.items.split(',').uniq.join(',')
		end
		respond_with(@orders)
	end

	def show
		@order = Order.find(params[:id])
		@items_arr = @order.items.split(',')
		@prices_arr = @order.prices.split(',')

		# Get items_arr_with_prices
		@items_arr_with_prices = []
		@items_arr.each_with_index { |e,i| @items_arr_with_prices.push(e + ':￥' + @prices_arr[i])}

		 # weird emuration to get occurences in an array
		@items_arr_with_prices_counts = @items_arr_with_prices.chunk{|y| y}.map{|y, ys| [y, ys.length]}

		@sum = @prices_arr.map { |e| e.to_f }.inject(:+)
	end

	def new
		@order = Order.new
		respond_with(@order)
	end

	def edit
		if @admin
			@order = Order.find(params[:id])
		else
			redirect_to '/pages/1'	
		end
	end

	def create
		if @current_user
			@order = Order.new(params[:order])
			@order.user = @current_user
			@order.tel = @current_user.tel
			@order.address = @current_user.address
			@order.user_name = @current_user.name
			@items_arr = @order.items.split(',')
			@prices_arr = []
			@items_arr.map { |e| @prices_arr.push(Menu.find_by_name(e).price) }
			@order.prices = @prices_arr.join(',')
			result = @order.save
		end

		if result
			path = order_path(@order)
			flash[:notice] = '恭喜您成功订餐，坐等美味自动送上门来吧！' 
			cookies.delete :_items
		else
			path = '/users/login'
		end
		redirect_to path
	end

	def update
		@order = Order.find(params[:id])
		flash[:notice] = '订单已经成功修改！' if @order.update_attributes(params[:order])
		respond_with(@order)
	end

	def destroy
		if @admin
			@order = Order.find(params[:id])
			@order.destroy
		else
			redirect_to '/pages/1'	
		end
	end
end
