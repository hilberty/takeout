// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require underscore
//= require backbone
//= require backbone_rails_sync
//= require backbone_datalink
//= require backbone/takeout
//= require_tree .

$(document).ready(function(){

	var _items = new Array()
	var _uniqItems
	var _item

	// init _items
	if ($.cookie('_items')!=null){
		_items = $.cookie('_items').split(',')
	}

	$('.buy').click(function(){
		_item = $(this).prev('span').children('a:first').html()
		buy(_item)
	})

	$('#reset').click(function(){
		resetCart()
	})

	$('#confirm-buy').click(function(){
		if(_items.length!=0){
			$('#order_items').attr('value', _items.join(','))
			$('#new_order').submit()
		}else {
			alert('亲，不要捉急，还没选餐呢')
		}
	})

	// Show cart
	refreshCart()

	// Functions
	function buy(item){
		_items.push(item)
		$.cookie('_items',_items.join(','))
		refreshCart()
	}

	function cancelBuy(item){
		var k=_.lastIndexOf(_items,item) // Get last index of item
		_items.splice(k,1) // Remove the last item from _items
		_items = _.compact(_items)
		$.cookie('_items',_items.join(','))
		if (_items.length===0) {
			$.removeCookie('_items')
		};
		refreshCart()
	}

	function deleteItem (item) {
		// alert(_items.length)
		// return
		cancelBuy(item)
	}

	function resetCart(){
 	 	_items.splice(0, _items.length)
 	 	$.removeCookie('_items')
 	 	refreshCart()
	}

	function refreshCart () {
		$('#cart').html('') // Reset Cart before appending _items

		if (_items.length) {
			_uniqItems = _.union(_items) // Remove duplicates
			for (var i = _uniqItems.length - 1; i >= 0; i--) {
				$('#cart').append('<li><span>' + _uniqItems[i] + '</span>: <span class="label">' + 
					countInArr(_items, _uniqItems[i]) + 
					'</span><i class="icon-plus-sign pull-right buy-plus"></i><i class="icon-minus-sign pull-right buy-minus"></i></li>')
			};
			
		};
		
		$('.buy-plus').click(function() {
			var plusItem = $(this).prev('span').prev('span').html()
			buy(plusItem)
		})
		
		$('.buy-minus').click(function() {
			var minusItem = $(this).closest('li').children('span').eq(0).html()
			var count = $(this).closest('li').children('span').eq(1).html()
			if (count === '1') {
				deleteItem(minusItem)
			}else {
				cancelBuy(minusItem)
			}
		})

	}

	function countInArr (arr, ele) {
		var eleCounts=0
		for (var j = arr.length - 1; j >= 0; j--) {
			if (arr[j]===ele) {
				eleCounts++
			};
		};
		return eleCounts
	}
})