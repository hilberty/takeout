class Takeout.Models.User extends Backbone.Model
  paramRoot: 'user'

  defaults:
    address: null
    name: null
    tel: null

class Takeout.Collections.UsersCollection extends Backbone.Collection
  model: Takeout.Models.User
  url: '/users'
