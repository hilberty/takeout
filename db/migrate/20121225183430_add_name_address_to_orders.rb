class AddNameAddressToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :user_name, :string
    add_column :orders, :address, :string
  end
end
